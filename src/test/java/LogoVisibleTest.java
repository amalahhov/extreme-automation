import org.junit.Test;

import static org.junit.Assert.*;


public class LogoVisibleTest {

    LogoVisible logoVisible = new LogoVisible();

    @Test
    public void isLogoVisible() throws Exception {

        boolean result = logoVisible.isLogoVisible();
        assertTrue(result);
    }

}