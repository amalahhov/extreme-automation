

import org.junit.Test;

import static org.junit.Assert.*;

public class RandIntTestFalse {

    RandInt randInt = new RandInt();

    @Test
    public void getRandomNumber() throws Exception {
        int result = randInt.getRandomNumber(0, 9999);
        assertFalse(result > 9999);
    }

    @Test
    public void testGetRandomNumber() throws Exception {
        int result2 = randInt.getRandomNumber(0, 9999);
        assertNotNull(result2);
    }

}