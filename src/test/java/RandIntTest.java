
import org.junit.Test;

import static org.junit.Assert.*;

public class RandIntTest {
    @Test
    public void testRandomNumber() {

        RandInt randInt = new RandInt();
        int result = randInt.getRandomNumber(0, 9999);
        int notRand = 15;

        for (int i = 0; i < 10; i++) {
            assertNotSame(notRand, result);
        }

    }

}