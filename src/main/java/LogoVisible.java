import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LogoVisible {
    public boolean isLogoVisible() {

        String exePath = "C:\\Users\\Andrei\\IdeaProjects\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);

        WebDriver driver = new ChromeDriver();
        driver.get("http://www.fob-solutions.com");

        WebElement logo = driver.findElement(By.className("company_logo"));
        boolean status = logo.isDisplayed();

        driver.close();
        driver.quit();

        return status;
    }
}
